#!/bin/bash

set -euo pipefail


# Wait for PostgreSQL to be up before starting
until PGPASSWORD=$SOGO_DB_PWD psql -h "postgres" -U "sogo" -c '\q'
do
	echo "Postgres is unavailable - sleeping"
	sleep 1
done

# Start SOGo.
/usr/local/sbin/sogod \
	-WONoDetach YES \
	-WOLogFile - \
	-WOPort 0.0.0.0:20000
