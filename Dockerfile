FROM debian:11-slim

EXPOSE 20000

ENV SOGO_VERSION 5.5.1
ENV FLAP_VERSION 1

ENV SOPE_DL_URL https://github.com/inverse-inc/sope/archive/SOPE-$SOGO_VERSION.tar.gz
ENV SOGO_DL_URL https://github.com/inverse-inc/sogo/archive/SOGo-$SOGO_VERSION.tar.gz

# mkdir fixes gitlab auto devop build
RUN mkdir -p /var/run && \
    # Allow installation of postgresql-client
    sed -i '/path-exclude \/usr\/share\/man/d' /etc/dpkg/dpkg.cfg.d/docker && \
    # Install build and runtime dependencies
    apt-get update && \
    apt-get install -y \
    build-essential \
    curl \
    gobjc \
    gnustep-base-common \
    gnustep-make \
    libcurl4            libcurl4-openssl-dev \
    # libglib2.0          libglib2.0-dev \
    libgnustep-base1.27 libgnustep-base-dev \
    liblasso3           liblasso3-dev  \
    libldap-2.4-2       libldap2-dev \
    libmemcached11      libmemcached-dev \
    # libmariadbclient18  libmariadbclient-dev-compat \
    libpq5              libpq-dev \
    libsodium23         libsodium-dev \
    libssl1.1           libssl-dev \
    libwbxml2-0         libwbxml2-dev \
    libxml2             libxml2-dev \
    libytnef0           libytnef0-dev \
    libzip4             libzip-dev \
    pkg-config \
    # Used to wait for postgreSQL to be available.
    postgresql-client \
    # pkg-config \
    python && \
    # Install SOPE
    mkdir /tmp/sope && \
    curl -SLf $SOPE_DL_URL | tar -zx --strip-components 1 -f - -C /tmp/sope && \
    cd /tmp/sope && \
    ./configure --with-gnustep && \
    make && \
    make install && \
    # Install SOGo
    mkdir /tmp/sogo && \
    curl -SLf $SOGO_DL_URL | tar -zx --strip-components 1 -f - -C /tmp/sogo && \
    cd /tmp/sogo && \
    ./configure --enable-saml2 && \
    make && \
    make install && \
    # Create sogo user
    useradd -ms /bin/bash sogo && \
    mkdir /var/run/sogo && \
    chown sogo:sogo /var/run/sogo && \
    # Create directory for draft mail.
    mkdir -p /var/spool/sogo && \
    chown sogo:sogo /var/spool/sogo && \
    # Clean image
    apt-get remove -y \
    build-essential \
    curl \
    gobjc \
    libcurl4-openssl-dev \
    # libglib2.0-dev \
    libgnustep-base-dev \
    liblasso3-dev \
    libldap2-dev \
    libmemcached-dev \
    libpq-dev \
    libsodium-dev \
    libssl-dev \
    libwbxml2-dev \
    libxml2-dev \
    libytnef0-dev \
    libzip-dev \
    # pkg-config \
    python && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER sogo

ENV LD_LIBRARY_PATH /usr/local/lib:/usr/local/lib/sogo

COPY ./docker-entrypoint.sh /docker-entrypoint.sh

CMD ["/docker-entrypoint.sh"]
